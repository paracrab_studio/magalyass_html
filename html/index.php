
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Bootstrap 101 Template</title>
        <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/main.css" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>-->
    <![endif]-->




</head>
<body>
<header>
   <div id="top">
       <div class="container">
           <div class="row">
               <nav class="navbar-default navbar " role="navigation" >
                   <div class="navbar-header">
                       <button type="button" data-target="#navbar-collapse" data-toggle="collapse" class="navbar-toggle">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                       </button>
                       <a id="logo" href="#">SITE NAME</a>
                   </div>
                  <div id="navbar-collapse" class="collapse navbar-collapse">

                   <ul class="nav navbar-nav navbar-right">
                       <li><a href="index.php">HOME</a></li>
                       <li><a href="#">ABOUT</a></li>
                       <li><a href="#">SERVICES</a></li>
                       <li><a href="#">PORTFOLIO</a></li>
                       <li><a href="#">CONTACT</a></li>
                   </ul>
                   </div>
               </nav>
           </div>
       </div>
   </div>
   <div class="container-fluid">
       <div id="top-carousel" class="carousel slide carousel-slideWobble" data-ride="carousel" data-interval="0">
           <div class="carousel-inner">
               <div class="item active">
                   <!--<img src="images/banner.jpg" alt="Адаптивные элементы для сайта">-->
                    <div class="carousel-caption">
                       <div class="slide">
                           <p class="text-one">CLIVUAM TINC</p>
                           <div class="clearfix"></div>
                           <p class="text-one">NUNC DIGNISSIM RISUS ID METUS</p>
                           <div class="clearfix"></div>
                           <p><a href="index.php">http://ovallpaperswide.com/</a></p>
                       </div>
                   </div>
               </div>
               <div class="item">
                   <!--<img src="images/banner.jpg" alt="Адаптивный дизайн для сайта">-->
                   <div class="carousel-caption">
                       <div class="slide">
                           <p class="text-one">CLIVUAM TINC</p>
                            <div class="clearfix"></div>
                           <p class="text-one">NUNC DIGNISSIM RISUS ID METUS</p>
                           <p><a href="index.php">http://ovallpaperswide.com/</a></p>
                       </div>
                   </div>
               </div>
               <div class="item">
                   <!--<img src="images/banner.jpg" alt="Адаптивные шаблоны для сайта">-->
                   <div class="carousel-caption">
                       <div class="slide">
                           <p class="text-one">CLIVUAM TINC</p>
                           <div class="clearfix"></div>
                           <p class="text-one">NUNC DIGNISSIM RISUS ID METUS</p>
                           <p><a href="index.php">http://ovallpaperswide.com/</a></p>
                       </div>
                   </div>
               </div>
           </div>
               <a class="left carousel-control half-round-left" href="#top-carousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
               <a class="right carousel-control half-round-right" href="#top-carousel" data-slide="next"><i class="fa fa-chevron-right"></i></a>
       </div>
   </div>
</header>
<div id="content" style="width: 100%">
    <div id="top-content">
        <div class="container">
            <div class="row">
                <div class=" top-block-post col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="round"> <i class="fa fa-rocket"></i></div>
                    <h4>ALIGUAM TINCIDIN</h4>
                    <span class="text-top-post">
                        Donec nec justo eget felis facialis fermentum. Aenean porttitor mausis sit amet
                        orci. Aenean dgnissim pellenteg ==><a href="index.php">more</a>
                    </span>
                </div>
                <div class="top-block-post col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="round"><i class="fa fa-shield "></i></div>
                    <h4>CRAS ORNARE TRISTIQU</h4>
                    <span class="text-top-post">
                        Donec nec justo eget felis facialis fermentum. Aenean porttitor mausis sit amet
                        orci. Aenean dgnissim pellenteg ==><a href="index.php">more</a>
                    </span>
                </div>
                <div class="top-block-post col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                    <div class="round"><i class="fa fa-fire-extinguisher"></i></div>
                    <h4>VIVAMUS VESTIBULUM</h4>
                    <span class="text-top-post">
                        Donec nec justo eget felis facialis fermentum. Aenean porttitor mausis sit amet
                        orci. Aenean dgnissim pellenteg ==><a href="index.php">more</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="center-content">
            <div class="row">
                <div  class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pull-left main-center-text">
                    DONES NEC JUSTO EGET FELIS FAGILISIS FERMENTUM. ALIQUAM PORTTITORSIT AMET ORCI.
                    AENEAN DIGNISSIM PELLENTESQUE FELIS
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-right main-photo">
                    <img id="main-photo" class="img-responsive pull-right" src="images/bird.gif" alt="bird"/>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 pull-left red-text">
                         Civamus vestibulum nulla nec ante
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 post-center pull-left">
                     <h4>Sed egestos anteet</h4>
                     <span class="post-center-text">
                     Eros pede semper est, vitoe luctus metus libero eu augue. Morbi puris libero, faucibuc adipiscing, commodo guis, gravida ld, est.
                     Set lectuc Praesent elementum hendrerit tortor. Ultrices sagittis, mi megue eyismod dui , ue pulvinar nunc sapied arnare nisl.
                     </span>
                     <button type="button" class="btn button">MORE</button>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 post-center pull-right">
                     <h4>Aenean egestos anteet </h4>
                     <span class="post-center-text">
                     Eros pede semper est, vitoe luctus metus libero eu augue. Morbi puris libero, faucibuc adipiscing, commodo guis, gravida ld, est.
                     Set lectuc Praesent elementum hendrerit tortor. Ultrices sagittis, mi megue eyismod dui , ue pulvinar nunc sapied arnare nisl.
                     </span>
                     <button type="button" class="btn button">MORE</button>
                </div>
            </div>
            <div class="row">
                <div id="red-block" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="container-black-round col-lg-1 col-md-2 col-sm-2 col-xs-2">
                        <div id="black-round"><i class="fa fa-user "></i> </div>
                    </div>
                    <div class="left-red-block col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <h5>DONEC NEC JUSTO EGET</h5>
                        <div class="citation">"Ede sempes est, vitae luctus mentus libero u augue. Morb sipo ipiscing, commodo guis, gravida..."
                            <span class="author"> - Kohn Doe/company name</span>
                        </div>
                    </div>
                    <div class="right-red-block col-lg-6 col-md-5 col-sm-5 col-xs-12 ">
                        <h5>MORBI VITAE LUCTUS</h5>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="email">Email</label>
                                <input type="email" id="email" class="form-control" placeholder="Enter your email id here...">
                                <button type="submit" id="submit" class="btn form-control">SUBSCRIBE<i class="fa fa-long-arrow-right"></i></button>
                            </div>
                        </form>
                     </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="post-bottom-block col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h4>ALIQUAM TINCIDUNT</h4>
                    <div class="post first-post">
                        <img class="photo" src="images/one.gif" alt="one"/>
                        <p class="text-photo">Donec nec justo eget felis facilisis fermen
                            tum. Aliguam porttitor
                            <a href="#">more =></a>
                        </p>
                    </div>
                    <div class="post">
                        <img class="photo" src="images/two.gif" alt="two"/>
                        <p class="text-photo">Donec nec justo eget felis facilisis fermen
                            tum. Aliguam porttitor
                            <a href="#">more =></a>
                        </p>
                     </div>
                    <div class="post last-post">
                        <img class="photo" src="images/three.gif" alt="three"/>
                        <p class="text-photo">Donec nec justo eget felis facilisis fermen
                            tum. Aliguam porttitor
                            <a href="#">more =></a>
                        </p>
                    </div>
                </div>
                <div class="post-bottom-block col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h4>NUNC DIGNISSIM RISU</h4>
                    <div class="form-block">
                        <div class="text-form">
                            Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus inetus liber.
                        </div>
                        <form id="form">
                            <input type="text" class="form-control border" placeholder="name...">
                            <input type="email" class="form-control border email" placeholder="email...">
                            <textarea class="form-control your-query" rows="3" placeholder="your query..."></textarea>
                            <button type="submit"  class=" form-control btn send">SEND<span class="glyphicon glyphicon-arrow-right" style="margin-left: 10px"></span></button>
                        </form>
                    </div>
                </div>
                <div class="post-bottom-block col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h4>ALIQUAM TINCIDUNT</h4>
                    <div class="address">
                         <i class="fa fa-home"></i>
                         <span class="title">Address</span>
                         <div class="clearfix"></div>
                         <span class="text-title">some block, street mumber, city name, country</span>
                    </div>
                    <div class="phone">
                        <div id="earphone">
                            <i class="fa fa-phone"></i>
                        </div>
                        <span class="title">Phone</span>
                        <div class="clearfix"></div>
                        <div class="text-title"><p>(000)777 777 7777</p>
                            <p>(000)888 888 8888</p>
                        </div>
                    </div>
                    <div class="email-us">
                         <i class="fa fa-envelope"></i>
                         <span class="title">Email us</span>
                         <div class="clearfix"></div>
                         <div class="text-title">
                             <p><a href="#"> info@sitename.com</a></p>
                             <p><a href="#"> sals@sinename.com</a></p>
                             <p><a href="#"> company@sitename.com</a></p>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </div>
<footer class="container-fluid">
       <div class="container">
       <div class="row">
           <div class="menu-footer">
               <nav class="navbar-default navbar col-lg-8 col-md-8 col-sm-8  pull-left" role="navigation">
                   <div class="navbar-header">
                       <button type="button" data-target="#navbar-collapse-footer" data-toggle="collapse" class="navbar-toggle">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                       </button>
                   </div>
                   <div id="navbar-collapse-footer" class="collapse navbar-collapse ">
                       <ul class="nav navbar-nav" style="padding: 0">
                           <li><a href="index.php">Home</a></li>
                           <li><a href="index.php">About</a></li>
                           <li><a href="index.php">Service</a></li>
                           <li><a href="index.php">Portfolio</a></li>
                           <li><a href="index.php">Contact</a></li>
                       </ul>
                   </div>
               </nav>
               <ul class="pull-right col-lg-4 col-md-4 col-sm-4">
                   <li>
                       <a href="index.php" class="round-red-min"><i class="fa fa-facebook position-icon"></i></a>
                       <a href="index.php" class="round-red-min"><i class="fa fa-twitter position-icon "></i></a>
                       <a href="index.php" class="round-red-min"><i class="fa  position-icon ">in</i></a>
                   </li>
               </ul>
           </div>
       </div>
    <div class="row">
       <p class="text-footer col-xs-12"><a href="#" >Designed by: www.alltemplancee ds.com/</a> images from: www.wallpapeawide.com/ Copyright(c) website name</p>
    </div>
       </div>


<script src="js/bootstrap.min.js"></script>

<script type="text/javascript">
    function setSliderButtonPosition(){
        var windowWidth = jQuery(window).width();
        var topCarousel = jQuery('#top-carousel');
        var slideWidth = topCarousel.find('.slide').width();
        topCarousel.find('.carousel-control.left').css('left', (windowWidth - slideWidth)/2 + 'px');
        topCarousel.find('.carousel-control.right').css('left', ((windowWidth - slideWidth)/2 + 28) + 'px');
    }
    jQuery(document).ready(setSliderButtonPosition);
    jQuery(window).resize(setSliderButtonPosition);
</script>
</body>
</html>