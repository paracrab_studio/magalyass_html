<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'project1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'T;x1(^M|.{k@p)eb}6:BYUH]-E+ZO@Ii+VfHl<M2~$VVu>qtlhP8r;l]q[Db5BSr');
define('SECURE_AUTH_KEY',  '|4gu|*DBK:Zc:r#?r+Td=Opd|=h,$f&q-?Hb$]e2JzNYz3SWplIJ*,6HX+jgY$BA');
define('LOGGED_IN_KEY',    '~h=(ncl[phW!A}DmS=~w`/1P+G{&QR&bq&>]sW?6$0kW+ZuiPI{xODX?mn7FG/22');
define('NONCE_KEY',        '#9&~I<3f)O+Zj+#JR4Qonhl0=Dk@$X@K1]+23P~PjFGE>|A5*&K|Dl:GuHFq$xW?');
define('AUTH_SALT',        '#ckMd#~ErweaPndQJJY0}Ub|YSz[~D|1DUQ>Ga?=B O]/GxY.`N6d8AdvWuoAr-{');
define('SECURE_AUTH_SALT', 'k^/dRGVO8`aHEzJ=jUTD8*jaIK@-*X{Ef7B!Y|{jC+HMsh-|iQpiG-~-jR1-N4T+');
define('LOGGED_IN_SALT',   '5+hWjy.BS&p:2trcjgFJGQ0vcG|=n)=6i4K!#c:[Y/h&I5pTs~bc^x:MRpH)@nu1');
define('NONCE_SALT',       'av .+A4)]^!j7.nP.lr-NzZpk|%pC^<*IC$aQqKl9UpKEORY/sto|P$!$|>;hl|8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
