<?php

function load_style_scripts(){
    wp_enqueue_script('script-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('script', get_template_directory_uri() . '/js/script.js');
    wp_enqueue_style('style-main', get_template_directory_uri() . '/css/main.css');
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css');
}
add_action('wp_enqueue_scripts','load_style_scripts');

/* Theme setup */
//add_action( 'after_setup_theme', 'wpt_setup' );
//if ( !function_exists( 'wpt_setup' ) ) {
//    function wpt_setup() {
//        register_nav_menu( 'menu', __( 'menu top', 'wptuts' ) );
//    }
//}
add_action('after_setup_theme', function(){
    register_nav_menus( array(
        'menu main' => 'menu top',
        'menu' => 'menu bottom'
    ));
});

add_action('init', 'feature');
function feature()
{
    $labels = array(
        'name' => 'Feature',
        'singular_name' => 'Feature',
        'add_new' => 'Add feature',
        'add_new_item' => 'Add new feature',
        'edit_item' => 'edit feature',
        'new_item' => 'New feature',
        'view_item' => 'view feature',
        'search_items' => 'search feature',
        'not_found' =>  'Feature not found',
        'not_found_in_trash' => 'В корзине feature не найдено',
        'parent_item_colon' => '',
        'menu_name' => 'Feature'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => array('title','editor','thumbnail', 'comments'),
        'taxonomies' => array('features')
    );
    register_post_type('feature',$args);
}


