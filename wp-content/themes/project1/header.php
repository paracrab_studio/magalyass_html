<!DOCTYPE html>
<html lang="en">
<head>
    <title>Study-project1</title>
    <meta charset="utf-8">
    <?php wp_head() ?>
</head>
<body>
<header>
    <div id="top">
        <div class="container">
            <div class="row">
                <nav class="navbar-default navbar " role="navigation" >
                    <div class="navbar-header">
                        <button type="button" data-target="#navbar-collapse" data-toggle="collapse" class="navbar-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a id="logo" href="index.php">SITE NAME</a>
                    </div>
                    <div id="navbar-collapse" class="collapse navbar-collapse">
                        <?php /* Primary navigation */
                        wp_nav_menu( array(
                            'menu' => 'menu top',
                            'depth' => 2,
                            'container' => false,
                            'menu_class' => 'nav navbar-nav navbar-right',
                            )
                        )
                        ?>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div id="top-carousel" class="carousel slide carousel-slideWobble" data-ride="carousel" data-interval="0">
            <div class="carousel-inner">
                <div class="item active">
                    <div class="carousel-caption">
                        <div class="slide">
                            <p class="text-one">CLIVUAM TINC</p>
                            <div class="clearfix"></div>
                            <p class="text-one">NUNC DIGNISSIM RISUS ID METUS</p>
                            <div class="clearfix"></div>
                            <p><a href="index.php">http://ovallpaperswide.com/</a></p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="carousel-caption">
                        <div class="slide">
                            <p class="text-one">CLIVUAM TINC</p>
                            <div class="clearfix"></div>
                            <p class="text-one">NUNC DIGNISSIM RISUS ID METUS</p>
                            <p><a href="index.php">http://ovallpaperswide.com/</a></p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="carousel-caption">
                        <div class="slide">
                            <p class="text-one">CLIVUAM TINC</p>
                            <div class="clearfix"></div>
                            <p class="text-one">NUNC DIGNISSIM RISUS ID METUS</p>
                            <p><a href="index.php">http://ovallpaperswide.com/</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <a class="left carousel-control half-round-left" href="#top-carousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
            <a class="right carousel-control half-round-right" href="#top-carousel" data-slide="next"><i class="fa fa-chevron-right"></i></a>
        </div>
    </div>
</header>