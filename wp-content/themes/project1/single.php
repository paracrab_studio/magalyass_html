<?php get_header()?>
<div id="content" style="width: 100%">
    <div id="top-content">
        <div class="container">
            <div class="row">

                    <div class=" top-block-post">

                            <h1 style="color: black; text-transform: uppercase"><?php the_title();?></h1>
                            <?php
                            while ( have_posts() ) : the_post();
                                the_content();
                            endwhile;?>
                    </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="center-content">
            <div class="row">
                <div  class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pull-left main-center-text">
                    DONES NEC JUSTO EGET FELIS FAGILISIS FERMENTUM. ALIQUAM PORTTITORSIT AMET ORCI.
                    AENEAN DIGNISSIM PELLENTESQUE FELIS
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-right main-photo">
                    <img id="main-photo" class="img-responsive pull-right" src="<?php bloginfo('template_url')?>/images/main.gif"  alt="bird"/>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 pull-left red-text">
                    Civamus vestibulum nulla nec ante
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 post-center ">
                    <?php query_posts('cat=9&showposts=2')?>
                    <?php while (have_posts()): the_post();?>
                    <div class="posts" style="width: 50%; height: auto; float: left; padding-right: 10px">
                        <h4><?php the_title(); ?></h4>
                        <span class="post-center-text">
                           <?php $content = get_the_content();
                           $trimmed_content = wp_trim_words( $content, 38, '<p><a class="btn button pull-left" href="'. get_permalink() .'">MORE</a></p>' );
                           echo $trimmed_content; ?>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                    <?php endwhile;?>
                </div>

            </div>
            <div class="row">
                <div id="red-block" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="container-black-round col-lg-1 col-md-2 col-sm-2 col-xs-2">
                        <div id="black-round"><i class="fa fa-user "></i> </div>
                    </div>
                    <div class="left-red-block col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <h5>DONEC NEC JUSTO EGET</h5>
                        <div class="citation">
                            <div class="citation">"Ede sempes est, vitae luctus mentus libero u augue. Morb sipo ipiscing, commodo guis, gravida..."
                                <span class="author"> - Kohn Doe/company name</span>
                            </div>
                        </div>
                    </div>
                    <div class="right-red-block col-lg-6 col-md-5 col-sm-5 col-xs-12 ">
                        <h5>MORBI VITAE LUCTUS</h5>
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="sr-only" for="email">Email</label>
                                <input type="email" id="email" class="form-control" placeholder="Enter your email id here...">
                                <button type="submit" id="submit" class="btn form-control">SUBSCRIBE<i class="fa fa-long-arrow-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">
                <div class="post-content-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="left col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding: 0">
                        <div class="post-bottom"> <h4>Aliguam tincidunt</h4></div>
                        <div class="post-bottom post-content-bottom">
                            <img class="photo"  src="<?php bloginfo('template_url')?>/images/one.gif"  alt="one photo"/>
                          <span class="text-photo">Donec nec justo eget felis facilisis fermen
                          tum. Aliguam porttitor
                          <p><a href="#">more =></a></p>
                          </span>
                        </div>
                        <div class="post-bottom post-content-bottom">
                            <img class="photo" src="<?php bloginfo('template_url')?>/images/two.gif"  alt="one photo"/>
                          <span class="text-photo">Donec nec justo eget felis facilisis fermen
                              tum. Aliguam porttitor
                              <p><a href="#">more =></a></p>
                          </span>
                        </div>
                        <div class="post-bottom post-content-bottom">
                            <img class="photo" src="<?php bloginfo('template_url')?>/images/three.gif"  alt="one photo"/>
                          <span class="text-photo">Donec nec justo eget felis facilisis fermen
                              tum. Aliguam porttitor
                              <p><a href="#">more =></a></p>
                          </span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="post-bottom"><h4>nunc dignissim risu</h4></div>
                        <div class="form-block">
                            <div class="text-form">
                                Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus inetus liber.
                            </div>
                            <form id="form">
                                <input type="text" class="form-control border" placeholder="name...">
                                <input type="email" class="form-control border email" placeholder="email...">
                                <textarea class="form-control your-query" rows="3" placeholder="your query..."></textarea>
                                <button type="submit"  class=" form-control btn send">SEND<i class="fa fa-long-arrow-right"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class=" left col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="post-bottom"> <h4>Aliguam tincidunt</h4></div>
                        <div class="post-bottom post-content-bottom">
                            <div class="contact">
                                <i class="fa fa-home"></i>
                                <span class="title">Address</span>
                                <div class="clearfix"></div>
                                <span class="text-title">some block, street mumber, city name, country</span>
                            </div>
                        </div>
                        <div class="post-bottom post-content-bottom">
                            <div class="contact">
                                <i class="fa fa-phone"></i>
                                <span class="title">Phone</span>
                                <div class="clearfix"></div>
                                <div class="text-title"><p>(000)777 777 7777</p>
                                    <p>(000)888 888 8888</p>
                                </div>
                            </div>
                        </div>
                        <div class="post-bottom post-content-bottom">
                            <div class="contact">
                                <i class="fa fa-envelope"></i>
                                <span class="title">Email us</span>
                                <div class="clearfix"></div>
                                <div class="text-title">
                                    <p><a href="#"> info@sitename.com</a></p>
                                    <p><a href="#"> sals@sinename.com</a></p>
                                    <p><a href="#"> company@sitename.com</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<?php get_footer();?>
