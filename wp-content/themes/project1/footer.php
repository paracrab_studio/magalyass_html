<footer class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="menu-footer">
                <nav class="navbar-default navbar col-lg-8 col-md-8 col-sm-8  pull-left" role="navigation">
                    <div class="navbar-header">
                        <button type="button" data-target="#navbar-collapse-footer" data-toggle="collapse" class="navbar-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar-collapse-footer" class="collapse navbar-collapse ">
                        <?php /* Primary navigation */
                        wp_nav_menu( array(
                                'menu' => 'menu bottom',
                                'menu_class' => 'nav navbar-nav',
                            )
                        )
                        ?>
                    </div>
                </nav>
                <ul class="pull-right col-lg-4 col-md-4 col-sm-4">
                    <li>
                        <a href="index.php" class="round-red-min"><i class="fa fa-facebook position-icon"></i></a>
                        <a href="index.php" class="round-red-min"><i class="fa fa-twitter position-icon "></i></a>
                        <a href="index.php" class="round-red-min"><i class="fa  position-icon ">in</i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <p class="text-footer col-xs-12"><a href="#" >Designed by: www.alltemplancee ds.com/</a> images from: www.wallpapeawide.com/ Copyright(c) website name</p>
        </div>
    </div>
</footer>
<?php wp_footer()?>
</body>
</html>