
function setSliderButtonPosition(){
    var windowWidth = jQuery(window).width();
    var topCarousel = jQuery('#top-carousel');
    var slideWidth = topCarousel.find('.slide').width();
    topCarousel.find('.carousel-control.left').css('left', (windowWidth - slideWidth)/2 + 'px');
    topCarousel.find('.carousel-control.right').css('left', ((windowWidth - slideWidth)/2 + 28) + 'px');
    }
jQuery(document).ready(setSliderButtonPosition);
jQuery(window).resize(setSliderButtonPosition);

